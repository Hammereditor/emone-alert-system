package rule;

public enum PriceType {
	WTDAVG, CLOSE, OPEN, LOW, HIGH, VOLUME
}