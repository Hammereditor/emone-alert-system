package exchange;

import java.util.List;

import support.MarketID;


public interface ChartFeedable<T>
{
	public void feedChartData(List<T> graph, MarketID mk) throws Exception;
}
