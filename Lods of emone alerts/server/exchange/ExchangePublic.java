package exchange;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import support.Candle;
import support.MarketID;
import support.Trade;
import support.Utils;




public abstract class ExchangePublic
{
	
	
	public abstract List<Trade> getTradeHistory(long startMs, long endMs, MarketID market) throws Exception;
	
	public abstract List<Candle> getCandlestickHistory(long startMs, long endMs, long periodMs, MarketID market) throws Exception;
	
	protected Object simpleJsonGET(String url) throws Exception
	{
		String json = null;
		
		try
		{
			json = Utils.getDataFromURL(url);
			//System.out.println(json);
		} catch (Exception e) {
			throw new Exception("Error while downloading trade history", e);
		}
		
		try
		{
			JSONParser p = new JSONParser();
			Object rootObj = p.parse(json);
			return rootObj;
		}
		catch (Exception e) {
			throw new Exception("Error while parsing JSON return result", e);
		}
	}
}
