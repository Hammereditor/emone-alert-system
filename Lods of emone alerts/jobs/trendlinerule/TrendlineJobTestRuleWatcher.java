package trendlinerule;

import rule.Rule;
import rule.RuleTemplateReplacements;
import rule.RuleWatcher;
import support.AlertMessage;
import support.ConsoleAlertMessage;
import support.Logs;
import support.MarketID;
import support.TemplatedAlertMessage;

public class TrendlineJobTestRuleWatcher extends RuleWatcher
{
	public TrendlineJobTestRuleWatcher(String name) {
		super(name);
	}

	public void doStrategy(MarketID mk)
	{
		for (AlertMessage a : super.getAlerts())
		{
			if (a instanceof ConsoleAlertMessage)
			{
				for (Rule r : super.getActivatedRules())
					if (r instanceof RuleTemplateReplacements)
						((ConsoleAlertMessage) a).putTemplateReplacements(((RuleTemplateReplacements)r).getTemplateReplacements());
			}
			
			try { a.send(); } catch (Exception e) { 
				Logs.log.error("TrendlineJobTestRuleWatcher.doStrategy(): error while sending alert message \'" + a.getTitle() + "\': " + e.getMessage());
				Logs.log.printException(e);
			}
		}
		
		Logs.log.debug("TrendlineJobTestRuleWatcher.doStrategy(): sent all alert messages and did all actions.");
	}
}
